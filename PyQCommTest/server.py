from PyQt4 import QtGui, QtCore
from PyQt4.QtNetwork import QHostAddress
from qcomm import Transport


class ServerWidget(Transport):
    statusLog = QtCore.pyqtSignal(QtCore.QString)

    def __init__(self, name):
        super(ServerWidget, self).__init__(self, host=self, name=name)
        #
        self.urlEdit = QtGui.QLineEdit('http://');
        self.statusButton = QtGui.QPushButton('Status of Url Processors');
        self.statTitle = QtGui.QLabel('status')
        self.statEdit = QtGui.QTextEdit("...");
        self.logTitle = QtGui.QLabel('log')
        self.logEdit = QtGui.QTextEdit("...");
        #
        self.urlEdit.returnPressed.connect(self.on_returnPressed)
        self.statusButton.pressed.connect(self.on_statusButtonPressed)
        self.statusLog.connect(self.on_statusLog)
        #
        self.mainWidget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.urlEdit);
        layout.addWidget(self.statusButton);
        layout.addWidget(self.statTitle);
        layout.addWidget(self.statEdit);
        layout.addWidget(self.logTitle);
        layout.addWidget(self.logEdit);
        self.mainWidget.setLayout(layout);
        #
        self.listen(hostAddress=QHostAddress.LocalHost, hostPort=9988)        
        #
        self.mainWidget.setWindowTitle(name);
        self.mainWidget.show()

    def on_returnPressed(self):
        url = self.urlEdit.text()
        if len(self.channels) == 0 :
            self.debugLog.emit("%s : no processors to handle %s" % (self.name, url))
            return
        for ch in self.channels.keys():
            self.debugLog.emit("%s : sending %s to %s" % (self.name, url, ch))
            self.channelByName(ch).invoke("processUrl(QString)", url)
    
    def on_debugLog(self, str):     #overload from Transport
        self.logEdit.append(str)

    def on_statusLog(self, str):
        self.statEdit.append(str)

    def on_newConnection(self, ch):     #overload from Transport
        ch.subscribe("debugLog(QString)")
        ch.subscribe("statusLog(QString)")

    def on_statusButtonPressed(self):
        if len(self.channels) == 0 :
            self.statusLog.emit("%s : no url processors connected" % (self.name))
            return
        for ch in self.channels.keys():
            self.whoIs(self.channelByName(ch))

    def on_whoIsReply(self, name, ver):     #overload from Transport
        Transport.on_whoIsReply(self, name, ver)
        self.statEdit.append('Status reply:  name:%s  version:%s'%(name, ver))

import sys
if __name__ == "__main__":
    a = QtGui.QApplication(sys.argv)
    srv = ServerWidget('url_server')
    a.exec_()
