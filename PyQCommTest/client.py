from PyQt4 import QtGui, QtCore, QtWebKit
from PyQt4.QtNetwork import QHostAddress
from qcomm import Transport



class ClientWidget(Transport):
    processUrl = QtCore.pyqtSignal(QtCore.QString)
    statusLog = QtCore.pyqtSignal(QtCore.QString)

    def __init__(self, name):
        super(ClientWidget, self).__init__(self, host=self, name=name)
        #
        self.mainWidget = QtGui.QWidget()
        self.taskLabel = QtGui.QLabel('no tasks running...')
        self.webView = QtWebKit.QWebView(self.mainWidget)
        self.timer = QtCore.QTime()
        #
        self.processUrl.connect(self.on_processUrl)
        self.statusLog.connect(self.on_statusLog)
        self.webView.loadStarted.connect(self.on_pageLoadStarted)
        self.webView.loadFinished.connect(self.on_pageLoadFinished)
        #
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.taskLabel)
        layout.addWidget(self.webView)
        self.mainWidget.setLayout(layout)
        self.mainWidget.setWindowTitle(name)
        self.mainWidget.show()
        #
        self.openChannel(peerAddress=QHostAddress.LocalHost, peerPort=9988)        
        
    def on_statusLog(self, s):
        self.notifySubscribers("statusLog(QString)", s)

    def on_processUrl(self, url):
        s = '%s : request for %s' % (self.name, url)
        self.taskLabel.setText(s)
        self.debugLog.emit(s)
        self.statusLog.emit(s)
        self.webView.load(QtCore.QUrl(url))

    def on_pageLoadStarted(self):
        self.timer.restart()

    def on_pageLoadFinished(self, success):
        s = ''
        if success:
            s = '%s : %s\n - received %s bytes in %s sec\n - title: %s' % \
            (self.name, self.webView.url().toString(), self.webView.page().bytesReceived(), self.timer.elapsed()/1000, self.webView.title())
            self.taskLabel.setText(self.webView.url().toString())
        else:
            s = '%s : error loading page' % self.name
        self.statusLog.emit(s)

import sys
if __name__ == "__main__":
    a = QtGui.QApplication(sys.argv)
    QtCore.qsrand(QtCore.QTime.currentTime().msec())
    srv = ClientWidget("url_processor_%s" % QtCore.qrand())
    a.exec_()
    
    