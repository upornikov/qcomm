import re
from PyQt4 import QtCore, QtNetwork
from PyQt4.QtCore import QCoreApplication, QObject, QString, QStringList, QByteArray, QTime, QDate
from PyQt4.QtNetwork import QTcpServer, QAbstractSocket, QTcpSocket, QHostAddress

class QComm_Exception(Exception): pass # TODO: Translate errors
QCOMM_ID = 123456789
QCOMM_VERSION = 1

#forward declarations
class Channel: pass
class Transport: pass   

class ChannelStatus:
    idle,connected,disconnected = range(3)
    pass

class Channel(QtCore.QObject):
#    debugLog = QtCore.pyqtSignal(QtCore.QString)
#    _ping = QtCore.pyqtSignal(QString, int)
#    _pong = QtCore.pyqtSignal(QString, int)
    _helloFrom = QtCore.pyqtSignal(QString)
    _subscribe = QtCore.pyqtSignal(QString, type(Channel))
    _unsubscribe = QtCore.pyqtSignal(QString, type(Channel))
    _close = QtCore.pyqtSignal(type(Channel))
    subscriptions = []

    def __init__(self, host, peerChannel=None, peerAddress=None, peerPort=None, peerSocketDescriptor=None):
        '''
        Create channel to peer in 3 ways:
        - peer is in the same process, accessed directly thru peer Channel object peerChannel
        - calling peer by its ip address: peerAddress:peerPort 
        - peer accepts tcp connection request by a socket descriptor: peerSocketDescriptor 
        '''
        super(QtCore.QObject, self).__init__()
        self.name = QString('')
        self.status = ChannelStatus.idle
        self.host = None
        self.peerChannel = None
        self.socket = None
        self.nextBlockSize = 0
        try:
            self.host = host
            if self.host: 
#                self._ping.connect(self.on_ping)
#                self._pong.connect(self.on_pong)
                self._subscribe.connect(self.on_subscribe)
                self._subscribe.connect(self.host._subscribe)
                self._unsubscribe.connect(self.on_unsubscribe)
                self._unsubscribe.connect(self.host._unsubscribe)
                self._close.connect(self.on_close)
                self._close.connect(self.host._close)
            if peerChannel:   #in_proc constructor
                self.peerChannel = peerChannel
                self.status = ChannelStatus.connected
                self.name = self.peerChannel.host.name
            elif peerAddress:   #by peer ip:port
                if peerPort == None: raise QComm_Exception ("missing port number")
                self.socket = QTcpSocket(self)
                self.status = ChannelStatus.connected
                self.socket.connected.connect(self.on_socketConnected)
                self.socket.disconnected.connect(self.close)
                self.socket.error.connect(self.on_error)
                self.socket.connectToHost(peerAddress, peerPort)
                if not self.waitForChannelReady(20000):
                    self.deinit()
            elif peerSocketDescriptor:  #accept tcp connection from peer
                self.socket = QTcpSocket(self)
                self.status = ChannelStatus.connected
                self.socket.disconnected.connect(self.close)
                self.socket.error.connect(self.on_error)
                if not self.socket.setSocketDescriptor(peerSocketDescriptor):
                    self.deinit()
                    return
                self.on_socketConnected()
                if not self.waitForChannelReady(20000):
                    self.deinit()
            else:
                return
        except Exception, e:
            if self.host:
                self.host.on_debugLog("%s : %s" % (self.host.name, e))    #emit replaced with straight call, to avoid debug messages from self
            else:
                qDebug(e)
            self.deleteLater()
            raise
        return

    def __del__(self):
        self.deinit()

    def waitForChannelReady(self, ms):
        t = QTime()
        t.start()
        while not self.name:
            if t.elapsed() > ms:
                return False
            QCoreApplication.flush()
            QCoreApplication.processEvents()
        return True

    def deinit(self):
        self.status = ChannelStatus.idle
        try:    
            for s in self.subscriptions:
                self._unsubscribe.emit(s, self)  #clean subscriptions from the channel and host Transport instance
            if self.socket:
                self.socket.disconnectFromHost()   
                self.socket.close()
                self.socket.deleteLater()
        except: pass
        self.socket = None

    def setPeerChannel(self, ch):
        self.peerChannel = ch
        if self.peerChannel:
            self.status = ChannelStatus.connected
            self.name = self.peerChannel.host.name
        else:
            self.status = ChannelStatus.idle

    def forward(self, signal, *args):
        if not self.host or self.status != ChannelStatus.connected: return
        sig = QtCore.QMetaObject.normalizedSignature(signal).data()
        if sig == "_subscribe(QString)":
            self._subscribe.emit(args[0], self)
        elif sig == "_unsubscribe(QString)":
            self._unsubscribe.emit(args[0], self)
        elif sig == "_helloFrom(QString)":
            self.on_helloFrom(args[0])
        elif sig == "_ping(QString,int)":
            self.on_ping(*args)
        elif sig == "_pong(QString,int)":
            self.on_pong(*args)
        elif sig == "_close()":
            self._close.emit(self)
        else:
#            self.host.on_debugLog("%s : forward %s(%s) from %s" % (self.host.name, signal, args, self.name))    #emit replaced with straight call, to avoid debug messages from self
            self.host.emit(QtCore.SIGNAL(sig), *args)
        return

    #client methods:
    def close(self):
        if self.status == ChannelStatus.connected:
            self.invoke("_close()")     #invoke closing peer channel
        try:
            self._close.emit(self)          #close this channel
        except: pass

    def invoke(self, signal, *args):
        if self.status != ChannelStatus.connected: return
#        self.host.on_debugLog("%s : invoke(%s, %s)" % (self.host.name, signal, args))
        sig = QtCore.QMetaObject.normalizedSignature(signal).data()
        if self.peerChannel:    #peer in proc->no need to marshall the signal, just call it from here
            self.peerChannel.forward(sig, *args)
            return
        elif self.socket:
            try:
                sargList = QString(sig).split(QtCore.QRegExp('[/(,/)]'), QString.SkipEmptyParts)
                block = QtCore.QByteArray()
                out = QtCore.QDataStream(block, QtCore.QIODevice.WriteOnly)
                out.setVersion(QtCore.QDataStream.Qt_4_1)
                out.writeInt(0)                 #for tcp channel create block size and id fields
                size_field_len = block.size()
                out.writeInt(QCOMM_ID)
                out.writeQString(sig)           #store signature
                del sargList[0]                 #take away the signal name, only arguments are mapped to vargs in the loop
                i = int(0)
                for sarg in sargList:
                    if len(args) == i:
                        raise QComm_Exception("Error invoke(%s, %s) : not enough arguments" % (signal, args))
                    if sarg == 'int':
                        out.writeInt(args[i])
                    elif sarg == 'QString':
                        out.writeQString(args[i])
                    elif sarg == 'QStringList':
                        out.writeQStringList(args[i])
                    elif sarg == 'QByteArray':
                        out.writeQVariant(args[i])
                    elif sarg == 'QTime':
                        out.writeQVariant(args[i])
                    elif sarg == 'QDate':
                        out.writeQVariant(args[i])
                    elif sarg == 'QVariant':
                        out.writeQVariant(args[i])
                    else:
                        raise QComm_Exception("Error invoke(%s, %s) : unsupported data type %s" % (signal, args, sarg))
                    i += 1
                out.device().seek(0)
                out.writeInt(block.size() - size_field_len)
                self.socket.write(block)
                self.socket.flush()
#                print "%s : done invoke(%s, %s)" % (self.host.name, signal, args)
            except Exception, e:
                self.host.on_debugLog("%s : %s" % (self.host.name, e))    #emit replaced with straight call, to avoid debug messages from self
                return
        else:
            return

    def subscribe(self, signal):
        if self.status == ChannelStatus.connected: self.invoke('_subscribe(QString)', signal)

    def unsubscribe(self, signal):
        if self.status == ChannelStatus.connected: self.invoke('_unsubscribe(QString)', signal)

    def ping(self, str, num):
        if self.status == ChannelStatus.connected:
            self.host.on_debugLog("%s : ping(%s, %s) to %s" % (self.host.name, str, num, self.name))
            self.invoke("_ping(QString,int)", str, num)

    #end of client methods:

    #slots
    def on_socketConnected(self):   #client socket initialization: in constructor connection to tcp server is just triggered
        if not self.socket:
            self.status = ChannelStatus.idle
            return
        if self.host:
            self._helloFrom.connect(self.on_helloFrom)
            self.socket.readyRead.connect(self.on_socketReadyRead)
            self.invoke("_helloFrom(QString)", self.host.name)

    def on_helloFrom(self, nm):
        self.name = nm

    def on_subscribe(self, signal):
        if self.status == ChannelStatus.connected: self.subscriptions.append(signal)

    def on_unsubscribe(self, signal):
        try: 
            del self.subscriptions[self.subscriptions.index(signal)];
        except: pass

    def on_ping(self, str, num):
        self.host.on_debugLog("%s : ping(%s, %s) from %s" % (self.host.name, str, num, self.name))
        retStr = QString('ponged: ' + str)
        self.invoke("_pong(QString,int)", retStr, num+1)

    def on_pong(self, str, num):
        self.host.on_debugLog("%s : pong(%s, %s) from %s" % (self.host.name, str, num, self.name))

    def on_error(self):
        self.host.on_debugLog("%s : %s" % (self.name, self.socket.errorString()))

    def on_close(self):
        self.deinit()
    
    def on_socketReadyRead(self):
        size_of_int = 4
        try:
            if self.status != ChannelStatus.connected or not self.socket or not self.socket.isValid() or self.socket.state() != QtNetwork.QAbstractSocket.ConnectedState:
                raise QComm_Exception("invalid socket")
            instream = QtCore.QDataStream(self.socket)
            instream.setVersion(QtCore.QDataStream.Qt_4_1)
            if self.nextBlockSize == 0:
                if self.socket.bytesAvailable() < size_of_int: 
                    return
                self.nextBlockSize = instream.readInt()
            if self.socket.bytesAvailable() < self.nextBlockSize:
                return
            id = instream.readInt()
            if id != QCOMM_ID:
                instream.skipRawData(self.socket.bytesAvailable())   #buffer is illegal
                raise QComm_Exception("invalid data packet signature")
            sig = instream.readQString()            #read signal signature
            sargList = sig.split(QtCore.QRegExp('[/(,/)]'), QString.SkipEmptyParts)
            if len(sargList) == 0:
                instream.skipRawData(self.nextBlockSize)   #illegal signature, unknown rpc structure
                raise QComm_Exception("invalid signal signature")
            del sargList[0]                     #take away the signal name, only arguments are mapped to vargs in the loop
            vargs = []    
            for sarg in sargList:
                if sarg == 'int':
                    vargs.append(instream.readInt())
                elif sarg == 'QString':
                    vargs.append(instream.readQString())
                elif sarg == 'QStringList':
                    vargs.append(instream.readQStringList())
                elif sarg == 'QByteArray':
                    data = instream.readQVariant()
                    vargs.append(data.toByteArray())
                elif sarg == 'QTime':
                    data = instream.readQVariant()
                    vargs.append(data.toTime())
                elif sarg == 'QDate':
                    data = instream.readQVariant()
                    vargs.append(data.toDate())
                elif sarg == 'QVariant':
                    vargs.append(instream.readQVariant())
                else:
                    instream.skipRawData(self.socket.bytesAvailable())   #buffer is illegal
                    raise QComm_Exception("unsupported data type %s" % sarg)
            self.forward(sig, *vargs)
        except Exception, e:
            self.host.on_debugLog("%s : %s" % (self.host.name, e))
        self.nextBlockSize = 0
        if self.socket.bytesAvailable() != 0:
            self.on_socketReadyRead()
        return
    #end of slots

class TcpChannelServer(QTcpServer):
    def __init__(self, host=None):
        super(QTcpServer, self).__init__()
        self.host = host
    def incomingConnection(self, socketDescriptor):
        self.host.acceptConnection(peerSocketDesc=socketDescriptor)


class Transport(QObject):
    version = int(QCOMM_VERSION)
    debugLog = QtCore.pyqtSignal(QString)
#    ping = QtCore.pyqtSignal(QString, QString, int)
#    pong = QtCore.pyqtSignal(QString, QString, int)
    _whoIsRequest = QtCore.pyqtSignal(QString, int)
    _whoIsReply = QtCore.pyqtSignal(QString, int)
    _newConnection = QtCore.pyqtSignal(Channel)
    _subscribe = QtCore.pyqtSignal(QString, type(Channel))
    _unsubscribe = QtCore.pyqtSignal(QString, type(Channel))
    _close = QtCore.pyqtSignal(type(Channel))

    def __init__(self, host=None, name=''):
        self.name = QString('')
        self.channels = {}
        self.subscriptions = {}
        super(QObject, self).__init__()
        self.host = host
        self.name = QString(name)
        self.srv = None
        self.debugLog.connect(self.on_debugLog)
#        self.ping.connect(self.on_ping)
#        self.pong.connect(self.on_pong)
        self._whoIsRequest.connect(self.on_whoIsRequest)
        self._whoIsReply.connect(self.on_whoIsReply)
        self._newConnection.connect(self.on_newConnection)
        self._subscribe.connect(self.on_subscribe)
        self._unsubscribe.connect(self.on_unsubscribe)
        self._close.connect(self.on_close)

    def __del__(self):
        for ch in self.channels.values():
            self.closeChannel(ch)
        del self.subscriptions
        del self.channels
        if self.srv:
            self.debugLog.emit(("%s : closing server on %s:%s")%(self.name, self.srv.serverAddress().toString(), self.srv.serverPort()))
            self.srv.close()
            self.srv.deleteLater()
            self.srv = None

    def openChannel(self, peerTransport=None, peerAddress=None, peerPort=None):         #client: setup tcp channel on sender, async call,
        '''
        Open channel to peer in 2 ways:
        - peer is in the same process, accessed directly thru its Transport base class instance
        - calling peer by its ip peerAddress:peerPort 
        '''
        ch = None
        try:
            if peerTransport:
                ch = Channel(host=self)
                peerChannel = peerTransport.acceptConnection(ch)
                ch.setPeerChannel(peerChannel)
                self.channels[ch.name] = ch
                self.debugLog.emit("%s : create local channel to %s" % (self.name, ch.name))
            elif peerAddress:
                if peerPort == None:
                    raise QComm_Exception("invalid port number")
                ch = Channel(host=self, peerAddress=peerAddress, peerPort=peerPort)
                if ch and ch.status == ChannelStatus.connected:
                    self.channels[ch.name] = ch
                    self.debugLog.emit("%s : create tcp channel to %s" % (self.name, ch.name))
                else:
                    if ch: del ch
                    ch = None
            else:
                raise QComm_Exception("invalid parameters")
        except Exception, e:
            self.debugLog.emit("%s : error opening channel: %s" % (self.name, e))
        return ch

    def acceptConnection(self, peerChannel=None, peerSocketDesc=None):    #host: incoming connection from peer
        ch = None
        try:
            if peerChannel:
                ch = Channel(host=self)
                ch.setPeerChannel(peerChannel)
                self.channels[ch.name] = ch
                self.debugLog.emit("%s : accept local channel from %s" % (self.name, ch.name))
                self._newConnection.emit(ch)
            elif peerSocketDesc:
                ch = Channel(host=self, peerSocketDescriptor=peerSocketDesc)
                if ch and ch.status == ChannelStatus.connected:
                    self.channels[ch.name] = ch
                    self.debugLog.emit("%s : accept tcp channel from %s" % (self.name, ch.name))
                    self._newConnection.emit(ch)
                else:
                    if ch: del ch
                    ch = None
            else:
                raise QComm_Exception("invalid parameters")
        except Exception, e:
            self.debugLog.emit("%s: error accepting channel: %s" % (self.name, e))
        return ch

    def listen(self, hostAddress, hostPort):
        '''listen(QHostAddress hostAddress, int hostPort)'''
        try:
            if self.srv:
                raise QComm_Exception("server is already running")
            self.srv = TcpChannelServer(host=self)
            if not self.srv.listen(hostAddress, hostPort):
                raise QComm_Exception(self.srv.errorString())
            self.debugLog.emit("%s : start listening on  %s:%s"%(self.name, hostAddress, hostPort))
            return True
        except Exception, e:
            self.debugLog.emit("%s : starting tcp server failed on %s:%s : %s" % (self.name, hostAddress, hostPort, e))
            del self.srv
            self.srv = None
            return False

    def closeChannel(self, ch):                 
        ch.close()

    def notifySubscribers(self, signal, *args):
        try:
            sig = str(signal)
            for ch in self.subscriptions[sig]:
                ch.invoke(sig, *args)
        except: pass

    def channelByName(self, name):
        try:
            return self.channels[QString(name)]
        except:
            return None

    def whoIs(self, channel):
        try:
            channel.invoke("_whoIsRequest(QString,int)", self.name, self.version)
        except: pass
    

    #slots

    def on_debugLog(self, str):
        s = "%s %s" % (QtCore.QDateTime.currentDateTime().toString("dd.MM-hh:mm:ss"), str);        
        self.notifySubscribers("debugLog(QString)", s)
        print s

    def on_newConnection(self, Channel):    #method to reload in Trasport children
        pass

    def on_whoIsRequest(self, senderName, senderVersion):
        try:
            self.debugLog.emit("%s : whoIs request from %s:%s" % (self.name, senderName, senderVersion))
            self.channels[senderName].invoke("_whoIsReply(QString,int)", self.name, self.version)
        except: pass

    def on_whoIsReply(self, senderName, senderVersion):
        self.debugLog.emit("%s : whoIs reply from %s:%s" % (self.name, senderName, senderVersion))

#    def on_ping(self, senderName, str, num):
#        self.debugLog.emit("%s : send ping(%s, %s) from %s" % (self.name, str, num, senderName))
#        self.channels[senderName].invoke("pong(QString,QString,int)", self.name, str, num+1)
#    def on_pong(self, senderName, str, num):
#        self.debugLog.emit("%s : received pong(%s, %s) from %s" % (self.name, str, num, senderName))

    def on_subscribe(self, signal, ch):
        sig = str(signal)
        self.debugLog.emit("%s : start subscription %s for %s" % (self.name, sig, ch.name))
        if not self.subscriptions.has_key(sig):
            self.subscriptions[sig] = []
        self.subscriptions[sig].append(ch)

    def on_unsubscribe(self, signal, ch):
        try:
            sig = str(signal)
            self.subscriptions[sig].remove(ch)
            self.debugLog.emit("%s : close subscription %s for %s" % (self.name, sig, ch.name))
        except: pass

    def on_close(self, ch):
        try:
            del self.channels[ch.name]
            self.debugLog.emit(("%s : closing channel to %s")%(self.name, ch.name))
            ch.deleteLater()
        except: pass



    
import sys
if __name__ == "__main__":
    a = QCoreApplication(sys.argv)
    
    t1 = Transport(None, "tr_1")
    t2 = Transport(None, "tr_2")
    ch12 = t1.openChannel(t2)
    ch12.ping("test string", 3)
    t1.whoIs(ch12) 
    ch21 = t2.openChannel(peerAddress=QHostAddress.LocalHost, peerPort=9988)
    ch21.ping("another test string", 7) 
    ch21.invoke("update_label(QString)", "Hello from QComm in python")
    
    a.exec_()



        