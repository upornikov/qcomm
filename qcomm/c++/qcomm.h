#ifndef QCOMM_H
#define QCOMM_H

#include <QWidget>
#include <QMutex>
#include <QWaitCondition>
#include <QMap>
#include <QSet>
#include <QPicture>
#include <QTime>
#include <QHostAddress>
#include <QTcpServer>
#include <QTcpSocket>

#define QCOMM_ID 123456789
#define QCOMM_VERSION  1

class Transport;

class Channel : public QObject
{
Q_OBJECT
    friend class Transport;
    typedef QSet<QString> StringSet;
    typedef void* lpvoid;
    enum channel_status {idle, connected, disconnected};
public:
    Channel(Transport* host, Channel* peer=NULL);                               //in proc channel constructor
    Channel(Transport* host, const QHostAddress& peerAddr, quint16 peerPort);   //tcp channel to peer
    Channel(Transport* host, int socketDescriptor);                             //tcp channel accepted from peer
    virtual ~Channel();

    void invoke(const char* signal, ...);
    void subscribe(const char* signal);         //client method to subscribe to peer signal
    void unsubscribe(const char* signal);       //client method to unsubscribe from peer signal
    void ping(const char* str, int num);           //client method to send ping to peer
public slots:
    void close();                               //client method to close the channel at both peers


protected:
    void invoke(const char* signal, va_list valist);
    void forward(const char* signal, void** args);
    bool waitForChannelReady(qint16 ms);
    void setPeerChannel(Channel* pCh);

signals:
//    void _ping(QString s, int num);                         //ping/pong are special qcomm signals: slots are called directly from forward()
//    void _pong(QString s, int num);
//    void debugLog(QString s);                               //client signal: moved to host to avoid recursion

    void _helloFrom(QString nm);
    void _subscribe(const char* signal, Channel* ch);
    void _unsubscribe(const char* signal, Channel* ch);
    void _close(Channel* ch);
protected slots:
    void deinit();
    void on_ping(QString s, int num);
    void on_pong(QString s, int num);
    void on_helloFrom(QString nm);                          //host slot
    void on_subscribe(const char* signal);                  //host slot
    void on_unsubscribe(const char* signal);                //host slot
    void on_close();                                        //host slot
    void on_error();
    void on_socketConnected();
    void on_socketReadyRead();

public:
    QString name;

protected:
    Transport* host;        //encapsulating Transport class instance
    channel_status status;
    Channel* peerChannel;   //in_proc peer channel pointer
    QTcpSocket* socket;    //tcp socket to peer
    StringSet subscriptions;
    int nextBlockSize;  //size of data block in socket databuffer
};

class TcpChannelServer;

class Transport : public QWidget
{
Q_OBJECT
    typedef QSet<Channel*> ChannelSet;
    typedef QMap<QString, ChannelSet> SubscriptionsMap;
    typedef SubscriptionsMap::iterator SubscriptionsMapIt;
public:
    int version;
    QWidget *host;
    QString name;
    QMap<QString, Channel*> channels;

    Transport(QWidget* h=NULL, const char* nm=NULL);
    virtual ~Transport();

    //client: setup new connection channel on sender
    Channel* openChannel(Transport* peer);
    Channel* openChannel(const QHostAddress& peerIp, quint16 peerPort);
    //client: close the channel from both peers
    void closeChannel(Channel* ch);

    //start TcpChannelServer to manage tcp channels for incoming requests
    bool listen(const QHostAddress& addr, quint16 port);
    //host: accept incoming connection from inproc peer
    Channel* acceptConnection(Channel* peerCh);
    //host: accept incoming connection from tcp peer
    Channel* acceptConnection(int socketDescriptor);
    Channel* channelByName(const char* name);
    void whoIs(Channel* peerCh);

protected:
    //host should call the method (e.g. from the signal slot), if host allows the signal be sent to subscribers
    void notifySubscribers(const char* signal, ...);

public:
protected:
    SubscriptionsMap subscriptions;

    TcpChannelServer* srv;  //tcp server to accept tcp requests and to spawn new tcp channels

signals:
    void debugLog(QString s);
    void _whoIsRequest(QString, int);
    void _whoIsReply(QString, int);
    void _newConnection(Channel* ch);
    void _subscribe(const char* signal, Channel* ch);
    void _unsubscribe(const char* signal, Channel* ch);
    void _close(Channel* ch);

protected slots:
    void on_whoIsRequest(QString senderName, int senderVersion);
    void on_whoIsReply(QString senderName, int senderVersion);
    void on_newConnection(Channel* ch);
    void on_subscribe(const char* signal, Channel* ch);
    void on_unsubscribe(const char* signal, Channel* ch);
    void on_close(Channel* ch);
public slots:
    void on_debugLog(QString s);
};

class TcpChannelServer : public QTcpServer
{
Q_OBJECT
public:
    TcpChannelServer(Transport *host = 0);
    Transport* host;        //encapsulating Transport class instance

protected:
    void incomingConnection(int socketDescriptor);
signals:

};


#endif	//QCOMM_H
