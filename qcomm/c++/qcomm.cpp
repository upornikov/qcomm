#include <QTimer>
#include <QVariant>
#include <QCoreApplication>
#include "qcomm.h"

//in proc channel constructor
Channel::Channel(Transport* host, Channel* peer):host(host),name(NULL),status(idle)
        ,peerChannel(peer)
        ,socket(NULL),nextBlockSize(0)
{
    if( peerChannel )
    {
        status = connected;
        name = peerChannel->host->name;
    }
    if( host )
    {
//        connect(this, SIGNAL(_ping(QString,int)), this, SLOT(on_ping(QString,int)));
//        connect(this, SIGNAL(_pong(QString,int)), this, SLOT(on_pong(QString,int)));
//        connect(this, SIGNAL(debugLog(QString)), host, SLOT(on_debugLog(QString)));
        connect(this, SIGNAL(_subscribe(const char*,Channel*)), this, SLOT(on_subscribe(const char*)));
        connect(this, SIGNAL(_subscribe(const char*,Channel*)), host, SIGNAL(_subscribe(const char*,Channel*)));
        connect(this, SIGNAL(_unsubscribe(const char*,Channel*)), this, SLOT(on_unsubscribe(const char*)));
        connect(this, SIGNAL(_unsubscribe(const char*,Channel*)), host, SIGNAL(_unsubscribe(const char*,Channel*)));
        connect(this, SIGNAL(_close(Channel*)), this, SLOT(on_close()));
        connect(this, SIGNAL(_close(Channel*)), host, SIGNAL(_close(Channel*)));
    }
}

//tcp channel to peer
Channel::Channel(Transport* host, const QHostAddress& peerAddr, quint16 peerPort):host(host),name(NULL),status(idle)
        ,peerChannel(NULL)
        ,socket(NULL),nextBlockSize(0)
{
    QTcpSocket* soc = new QTcpSocket(this);
    if( !soc ) return;
    socket = soc;
    status = connected;
    //connection is asynchronous->all connections should be set in on_connected callback
    connect(socket, SIGNAL(connected()), this, SLOT(on_socketConnected()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(on_error()));
    socket->connectToHost(peerAddr, peerPort);
    if( !waitForChannelReady(20000) )
        deinit();
}

//tcp channel accepted from peer
Channel::Channel(Transport* host, int socketDescriptor):host(host),name(NULL),status(idle)
        ,peerChannel(NULL)
        ,socket(NULL),nextBlockSize(0)
{
    QTcpSocket* soc = new QTcpSocket(this);
    if( !soc ) return;
    if( !soc->setSocketDescriptor(socketDescriptor) )
    {
        soc->deleteLater();;
        return;
    }
    socket = soc;
    status = connected;
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(on_error()));
    on_socketConnected();
    if( !waitForChannelReady(20000) )
        deinit();
}

void Channel::on_error()
{
    if( socket )
        host->on_debugLog(socket->errorString());
}

Channel::~Channel()
{
    deinit();
}

void Channel::deinit()
{
	status = idle;
	foreach(QString s, subscriptions)
		emit _unsubscribe(s.toAscii().data(), this);     //clean subscriptions from the channel and host Transport instance
        if( socket )
	{
            socket->disconnectFromHost();
            socket->close();
            socket->deleteLater();
            socket = NULL;
	}
}

void Channel::setPeerChannel(Channel* pCh)
{
    peerChannel = pCh;
    if( peerChannel )
    {
        status = connected;
        name  = peerChannel->host->name;
    }
    else
        status = idle;
}

//client socket initialization: in constructor connection to tcp server is just triggered
void Channel::on_socketConnected()
{
    if( !socket )
        return;
    if( host )
    {
        connect(this, SIGNAL(_subscribe(const char*,Channel*)), this, SLOT(on_subscribe(const char*)));
        connect(this, SIGNAL(_subscribe(const char*,Channel*)), host, SIGNAL(_subscribe(const char*,Channel*)));
        connect(this, SIGNAL(_unsubscribe(const char*,Channel*)), this, SLOT(on_unsubscribe(const char*)));
        connect(this, SIGNAL(_unsubscribe(const char*,Channel*)), host, SIGNAL(_unsubscribe(const char*,Channel*)));
        connect(this, SIGNAL(_close(Channel*)), this, SLOT(on_close()));
        connect(this, SIGNAL(_close(Channel*)), host, SIGNAL(_close(Channel*)));
        connect(this, SIGNAL(_helloFrom(QString)), this, SLOT(on_helloFrom(QString)));
        connect(socket, SIGNAL(readyRead()), this, SLOT(on_socketReadyRead()));
        connect(socket, SIGNAL(disconnected()), this, SLOT(close()));
        invoke("_helloFrom(QString)", host->name);
    }
}

void Channel::invoke(const char* signal, ...)
{
    va_list valist;
    va_start(valist, signal);
    invoke(signal, valist);
    va_end(valist);
    return;
}

void Channel::invoke(const char* signal, va_list valist)
{
    if( connected != status )
        return;
    QString sig = signal;
    QStringList sargList = sig.split(QRegExp("[/(,/)]"), QString::SkipEmptyParts);
    if( sargList.size() < 1 )
        return;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_1);
    if( socket )    //for tcp channel create block size and id fields
    {
        out << int(0) << int(QCOMM_ID);
    }
    lpvoid* vargs = new lpvoid[sargList.size()];    //for sig_slot return code + sig_slot args
    if( !vargs ) goto bye;
    vargs[0] = NULL;                                //sig_slot return code
    lpvoid* varg = vargs;

    if( socket )
        out << sig;
    if( sargList.size() > 1 )
    {
        //take away func signature, only arguments are parsed in the loop
        sargList.removeFirst();
        foreach(QString sarg, sargList)   //todo: !no arg validation and type check here
        {
            ++varg;
            sarg = sarg.trimmed();

            //todo: there should be quicker way to switch by sarg
            if( sarg == "int" )
            {
                int theArg = va_arg( valist, int);
                *varg = &theArg;
                if( this->socket ) out << theArg;   //marshall argument if peer is not in proc (socket)
            }
            else if( sarg == "QString" )
            {
                QString theArg = va_arg( valist, QString);
                *varg = &theArg;
                if( this->socket ) out << theArg;
            }
            else if( sarg == "QStringList" )
            {
                QStringList theArg = va_arg( valist, QStringList);
                *varg = &theArg;
                if( this->socket ) out << theArg;
            }
            else if( sarg == "QByteArray" )
            {
                QByteArray theArg = va_arg( valist, QByteArray);
                *varg = &theArg;
                if( this->socket ) out << QVariant(theArg);
            }
            else if( sarg == "QTime" )
            {
                QTime theArg = va_arg( valist, QTime);
                *varg = &theArg;
                if( this->socket ) out << QVariant(theArg);
            }
            else if( sarg == "QDate" )
            {
                QDate theArg = va_arg( valist, QDate);
                *varg = &theArg;
                if( this->socket ) out << QVariant(theArg);
            }
            else if( sarg == "QVariant" )
            {
                QVariant theArg = va_arg( valist, QVariant);
                *varg = &theArg;
                if( this->socket ) out << theArg;
            }
            else
            {
                qDebug() << QString("%1 : skip invoking %2 due to unsupported data type %3").arg(host->name).arg(signal).arg(sarg);     //debugLog loops here
                goto bye;
            }
        }
    }
    if( peerChannel )
        peerChannel->forward(signal, vargs);  //peer in proc->no need to marshall the signal, just call it from here
    else if( socket )
    {
        out.device()->seek(0);
        out << int(block.size() - sizeof(int));
        socket->write(block);
        socket->flush();
    }

bye:
    if( vargs ) delete [] vargs;
    return;
}

void Channel::forward(const char* signal, void** args)
{
    if( !host || connected != status )
        return;
    QByteArray sig = QMetaObject::normalizedSignature(signal);
    if( sig == "_subscribe(QString)" )
    {
        QByteArray subscription = QMetaObject::normalizedSignature((*(QString*)args[1]).toAscii().data());
        emit _subscribe(subscription.data(), this);
        return;
    }
    else if( sig == "_unsubscribe(QString)" )
    {
        QByteArray subscription = QMetaObject::normalizedSignature((*(QString*)args[1]).toAscii().data());
        emit _unsubscribe(subscription.data(), this);
        return;
    }
    else if( sig == "_helloFrom(QString)" )
    {
        on_helloFrom(*(QString*)args[1]);
        return;
    }
    else if( sig == "_ping(QString,int)" )
    {
        on_ping(*(QString*)args[1], *(int*)args[2]);
        return;
    }
    else if( sig == "_pong(QString,int)" )
    {
        on_pong(*(QString*)args[1], *(int*)args[2]);
        return;
    }
    else if( sig == "_close()" )
    {
        emit _close(this);
        return;
    }

    int signalId = host->metaObject()->indexOfSignal(sig);
    if (signalId == -1)
        return;

     //replaced with a straight call, to avoid debug messages from self
	//emit debugLog(QString("%1 : forward(%2) from %3").arg(host->name).arg(signal).arg(name));
    host->on_debugLog(QString("%1 : forward(%2) from %3").arg(host->name).arg(signal).arg(name));
    QMetaObject::activate(host, signalId, args);
}

//client method to close the channel at both peers
void Channel::close()
{
    if( connected == status )
        invoke("_close()");  //close and delete channel instance on peer host
    emit _close(this);       //close and delete channel on client; transport host must clean all references to the channel from subscription structures
}

//client method to subscribe to peer signal
void Channel::subscribe(const char* signal)
{
    if( connected != status )
        return;
    invoke("_subscribe(QString)", QString(signal));
}

//client method to unsubscribe from peer signal
void Channel::unsubscribe(const char* signal)
{
    if( connected != status )
        return;
    invoke("_unsubscribe(QString)", QString(signal));
}

//client method to send ping to peer
void Channel::ping(const char* str, int num)
{
    if( connected == status )
    {
        host->on_debugLog(QString("%1 : ping(%2, %3) to %4").arg(host->name).arg(str).arg(num).arg(name));
        invoke("_ping(QString,int)", str, num);
    }
}

void Channel::on_subscribe(const char* signal)
{
    if( connected != status )
        return;
    this->subscriptions.insert(signal);
}

void Channel::on_unsubscribe(const char* signal)
{
    this->subscriptions.remove(signal);
}

//void Channel::on_close(Channel* ch)
void Channel::on_close()
{
    deinit();
}

bool Channel::waitForChannelReady(qint16 ms)
{
    QTime t;
    t.start();
    while( name.isEmpty() )
    {
        if( t.elapsed() > ms )
            return false;
        QCoreApplication::flush();
        QCoreApplication::processEvents();
    }
    return true;
}

void Channel::on_ping(QString s, int num)
{
    host->on_debugLog(QString("%1 : ping(%2, %3) from %4").arg(host->name).arg(s).arg(num).arg(name));
    QString retStr(QString("ponged: ") + s);
    invoke("_pong(QString,int)", retStr, num+1);
}
void Channel::on_pong(QString s, int num)
{
    host->on_debugLog(QString("%1 : pong(%2, %3) from %4").arg(host->name).arg(s).arg(num).arg(name));
}


void Channel::on_helloFrom(QString nm)
{
    name = nm;
}

void Channel::on_socketReadyRead()
{
    if( connected != status || !socket
        || !socket->isValid() || socket->state() != QAbstractSocket::ConnectedState )
        return;

    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_4_1);
    if (nextBlockSize == 0) {
        if( socket->bytesAvailable() < sizeof(int))
            return;
        in >> nextBlockSize;
    }
    if( socket->bytesAvailable() < nextBlockSize )
        return;

    int id;
    in >> id;
    if( id != QCOMM_ID )  //illegal data, unknown structure, skip all
    {
        in.skipRawData(socket->bytesAvailable());   //buffer is illegal
        nextBlockSize = 0;
        return;
    }

    QString sig;
    in >> sig;

    QStringList sargList = sig.split(QRegExp("[/(,/)]"), QString::SkipEmptyParts);
    int n_vargs = sargList.size();
    if( n_vargs == 0 )
    {
        in.skipRawData(nextBlockSize);              //illegal signature, unknown rpc structure
        nextBlockSize = 0;
        return;
    }
    lpvoid* vargs = new lpvoid[n_vargs];            //for sig_slot return code + sig_slot args
    if( !vargs ) goto bye;
    memset(vargs, 0, sizeof(lpvoid)*n_vargs);
    vargs[0] = new int(0);                          //sig_slot return code
    lpvoid* varg = vargs;

    if( sargList.size() > 1 )
    {
        //take away func signature, only arguments are parsed in the loop
        sargList.removeFirst();
        foreach(QString sarg, sargList)   //todo: !no arg validation and type check here
        {
            ++varg;
            sarg = sarg.trimmed();

            //todo: there should be quicker way to switch by sarg
            if( sarg == "int" )
            {
                int* theArg = new int;
                in >> *theArg;
                *varg = theArg;
            }
            else if( sarg == "QString" )
            {
                QString* theArg = new QString;
                in >> *theArg;
                *varg = theArg;
            }
            else if( sarg == "QStringList" )
            {
                QStringList* theArg = new QStringList;
                in >> *theArg;
                *varg = theArg;
            }
            else if( sarg == "QByteArray" )
            {
                QVariant vArg;
                in >> vArg;
                QByteArray* theArg = new QByteArray(vArg.toByteArray());
                *varg = theArg;
            }
            else if( sarg == "QTime" )
            {
                QVariant vArg;
                in >> vArg;
                QTime* theArg = new QTime(vArg.toTime());
                *varg = theArg;
            }
            else if( sarg == "QDate" )
            {
                QVariant vArg;
                in >> vArg;
                QDate* theArg = new QDate(vArg.toDate());
                *varg = theArg;
            }
            else if( sarg == "QVariant" )
            {
                QVariant* theArg = new QVariant;
                in >> *theArg;
                *varg = theArg;
            }
            else
            {
                host->on_debugLog(QString("%1 : skip forwarding %2 due to unsupported data type %3").arg(host->name).arg(sig).arg(sarg));
                in.skipRawData(socket->bytesAvailable());   //buffer contains illegal type
                goto bye;
            }
        }
    }

    forward(sig.toAscii().data(), vargs);  //peer in proc->no need to marshall the signal, just call it from here

bye:
    nextBlockSize = 0;
    if( vargs && n_vargs )
    {
        for(int i=0; i<n_vargs; ++i)
        {
            if( vargs[i] )
            {
                delete vargs[i];
                vargs[i] = 0;
            }
        }

        delete [] vargs;
    }
    if( socket->bytesAvailable() != 0 )
        on_socketReadyRead();
    return;
}





Transport::Transport(QWidget* h, const char* nm):version(QCOMM_VERSION),host(h),name(nm),srv(NULL)
{
    connect(this, SIGNAL(debugLog(QString)), this, SLOT(on_debugLog(QString)));
    connect(this, SIGNAL(_whoIsRequest(QString, int)), this, SLOT(on_whoIsRequest(QString, int)));
    connect(this, SIGNAL(_whoIsReply(QString, int)), this, SLOT(on_whoIsReply(QString, int)));
    connect(this, SIGNAL(_newConnection(Channel*)), this, SLOT(on_newConnection(Channel*)));
    connect(this, SIGNAL(_subscribe(const char*, Channel*)), this, SLOT(on_subscribe(const char*, Channel*)));
    connect(this, SIGNAL(_unsubscribe(const char*, Channel*)), this, SLOT(on_unsubscribe(const char*, Channel*)));
    connect(this, SIGNAL(_close(Channel*)), this, SLOT(on_close(Channel*)));
}
Transport::~Transport()
{
    foreach(Channel *ch, channels)  //for QMap foreach accesses value component
       closeChannel(ch);
    subscriptions.clear();
    channels.clear();
    if( srv )
    {
        emit debugLog(QString("%1 : closing server on %2:%3").arg(name).arg(srv->serverAddress().toString()).arg(srv->serverPort()));
        srv->close();
        srv->deleteLater();
        srv = NULL;
    }
}

//client: setup new connection channel on sender
Channel* Transport::openChannel(Transport* peer)
{
    Channel* ch = new Channel(this);
    if( !ch )
        return NULL;
    Channel* peerCh = peer->acceptConnection(ch);   //inproc peer has to be triggered directly
    if( !peerCh )
    {
        delete ch;
        return NULL;
    }
    ch->setPeerChannel(peerCh);
    channels.insert(ch->name, ch);
    emit debugLog(QString("%1 : create local channel to %2").arg(name).arg(ch->name));
    return ch;
}
//host: incoming connection from inproc peer
Channel* Transport::acceptConnection(Channel* peerCh)
{
    Channel* ch = new Channel(this);
    if( !ch )
        return NULL;
    ch->setPeerChannel(peerCh);
    channels.insert(ch->name, ch);
    emit debugLog(QString("%1 : accept local channel from %2").arg(name).arg(ch->name));
    return ch;
}

Channel* Transport::channelByName(const char* name)
{
    return channels.value(name, NULL);
}

void Transport::whoIs(Channel* ch)
{
    ch->invoke("_whoIsRequest(QString,int)", name, version);
}

//client: setup tcp channel on sender, async call,
Channel* Transport::openChannel(const QHostAddress& peerAddr, const quint16 peerPort)
{
    Channel* ch = new Channel(this, peerAddr, peerPort);
    if( !ch )
        return NULL;

    if( ch->status != Channel::connected )
    {
        ch->deleteLater();
        return NULL;
    }
    channels.insert(ch->name, ch);
    emit debugLog(QString("%1 : create tcp channel to %2").arg(name).arg(ch->name));
    return ch;
}

//host: incoming connection from tcp peer
Channel* Transport::acceptConnection(int socketDescriptor)
{
    Channel* ch = new Channel(this, socketDescriptor);
    if( !ch )
        return NULL;

    if( ch->status != Channel::connected )
    {
        delete ch;
        return NULL;
    }
    channels.insert(ch->name, ch);
    emit debugLog(QString("%1 : accept tcp channel from %2").arg(name).arg(ch->name));
    return ch;
}

void Transport::closeChannel(Channel* ch)
{
    ch->close();
}

void Transport::on_debugLog(QString s)
{
    QString str = QString("%1 %2").arg(QDateTime::currentDateTime().toString("dd.MM-hh:mm:ss")).arg(s);
    qDebug() << str;
    notifySubscribers("debugLog(QString)", str);
}

void Transport::on_whoIsRequest(QString senderName, int senderVersion)
{
    emit debugLog(QString("%1 : whoIs request from %2:%3").arg(name).arg(senderName).arg(senderVersion));
    if( channels.find(senderName) != channels.end() )
        channels[senderName]->invoke("_whoIsReply(QString,int)", name, version);
}

//placeholder to be redefined by Transport children
void Transport::on_whoIsReply(QString senderName, int senderVersion)
{
    emit debugLog(QString("%1 : whoIs reply from %2:%3").arg(name).arg(senderName).arg(senderVersion));
}

//placeholder to be redefined by Transport children
void Transport::on_newConnection(Channel* ch)
{
    return;
}

void Transport::on_subscribe(const char* signal, Channel* ch)
{
    emit debugLog(QString("%1 : start subscription %2 for %3").arg(name).arg(signal).arg(ch->name));
    subscriptions[signal].insert(ch);
}
void Transport::on_unsubscribe(const char* signal, Channel* ch)
{
    SubscriptionsMapIt it = subscriptions.find(signal);
    if( it != subscriptions.end() )
    {
        emit debugLog(QString("%1 : close subscription %2 for %3").arg(name).arg(signal).arg(ch->name));
        it->remove(ch);
    }
}

void Transport::notifySubscribers(const char* signal, ...)
{
    SubscriptionsMapIt theSubscr = subscriptions.find(signal);
    if( theSubscr == subscriptions.end() )
        return;
    va_list valist;
    va_start(valist, signal);
    for(ChannelSet::iterator chit = theSubscr->begin(); chit != theSubscr->end(); chit++)
    {
        (*chit)->invoke(signal, valist);
    }
    va_end(valist);
    return;
}

void Transport::on_close(Channel* ch)
{
    emit debugLog(QString("%1 : closing channel to %2").arg(name).arg(ch->name));
    channels.remove(ch->name);
    ch->deleteLater();      //let ch->on_close to finish
}

bool Transport::listen(const QHostAddress& addr, quint16 port)
{
    srv = new TcpChannelServer(this);
    if( !srv ) return false;
    if( !srv->listen(addr, port) )
    {
        emit debugLog(QString("%1 : failed to start tcp server on  %2:%3: %4")\
                      .arg(name).arg(addr.toString()).arg(port).arg(srv->errorString()));
        srv->deleteLater();
        srv = NULL;
        return false;
    }
    emit debugLog(QString("%1 : start listening on %2:%3").arg(name).arg(addr.toString()).arg(port));
    return true;
}




TcpChannelServer::TcpChannelServer(Transport *host) : QTcpServer(host), host(host)
{
}

void TcpChannelServer::incomingConnection(int socketDescriptor)
{
    host->acceptConnection(socketDescriptor);
}
