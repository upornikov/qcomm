# -------------------------------------------------
# Project created by QtCreator 2010-03-14T14:34:27
# -------------------------------------------------
TARGET = QCommTest
CONFIG += console
CONFIG += qtestlib
QT += network
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += main.cpp \
    widgets.cpp \
    qcomm.cpp
HEADERS += qcomm.h \
    widgets.h
