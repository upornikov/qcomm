#include <QtGui/QApplication>
#include "widgets.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString fn[] = {"widget_%1", "widget_%1", "stat_%1", "debug_log_%1"};

    QDialog mainDlg;
    QTime t = QTime::currentTime();
    qsrand(t.msec());
    StringWidget w1(fn[0].arg(qrand()).toAscii().data());
    StringWidget w2(fn[1].arg(qrand()).toAscii().data());
    LogWidget stat(fn[2].arg(qrand()).toAscii().data());
    DebugLogWidget debug_log(fn[3].arg(qrand()).toAscii().data());

    QVBoxLayout layout;
    layout.addWidget(&w1);
    layout.addWidget(&w2);
    layout.addWidget(&stat);
    layout.addWidget(&debug_log);
    mainDlg.setLayout(&layout);
    mainDlg.setWindowTitle("Qt communication demo");


    Channel* ch_w12 = w1.openChannel(&w2);     //duplex channel: w12<->widget_1 to be used with ivoke()
    w1.whoIs(ch_w12);
    Channel* ch_stat1 = stat.openChannel(&w1);
    if( !ch_w12 || !ch_stat1 )
        goto err;

    //subscription thru local channels
    ch_stat1->subscribe("update_log(QString)"); //stat will catch update_log() signals from w1 (see on_update_label)

    //subscriptions thru tcp channels
    w2.listen(QHostAddress::LocalHost, 9988);
    Channel* ch_stat2 = stat.openChannel(QHostAddress::LocalHost, 9988);
    if( !ch_stat2 ) goto err;

    //subscribe to server
    ch_stat2->subscribe("update_log(QString)");  //now both String widgets will log their strings in stat

    //collect all debug logs into debug_log
    debug_log.listen(QHostAddress::LocalHost, 9900);
    Channel* ch_w1debug_log = w1.openChannel(QHostAddress::LocalHost, 9900);
    if( !ch_w1debug_log ) goto err;
    Channel* ch_w2debug_log = w2.openChannel(QHostAddress::LocalHost, 9900);
    if( !ch_w2debug_log ) goto err;
    Channel* ch_statdebug_log = stat.openChannel(QHostAddress::LocalHost, 9900);
    if( !ch_statdebug_log ) goto err;

    //server subscribes to clients
    for(QMap<QString, Channel*>::iterator it = debug_log.channels.begin(); it != debug_log.channels.end(); it++)
    {
        debug_log.channelByName(it.key().toAscii().data())->subscribe("debugLog(QString)");
    }

    mainDlg.show();
    return a.exec();

err:
    qDebug() << " Program failure!";
    return 1;
}
