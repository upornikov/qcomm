#ifndef WIDGETS_H
#define WIDGETS_H

#include <QtGui>
#include <QTextEdit>
#include "qcomm.h"


class StringWidget : public Transport
{
Q_OBJECT
public:
    StringWidget(const char* name);
    virtual ~StringWidget();

    QLineEdit* edit;
    QLabel* label;
    QLabel* title;
    QVBoxLayout *layout;
signals:
    void update_label(QString s);
public slots:
    void on_update_label(QString s);
    void on_returnPressed();
};


class LogWidget : public  Transport
{
Q_OBJECT
public:
    LogWidget(const char* name);
    virtual ~LogWidget();

    QTextEdit* edit;
    QLabel* label;
    QVBoxLayout *layout;
signals:
    void update_log(QString s);
public slots:
    void on_update_log(QString s);
};


class DebugLogWidget : public  Transport
{
Q_OBJECT
public:
    DebugLogWidget(const char* name);
    virtual ~DebugLogWidget();

    QTextEdit* edit;
    QLabel* label;
    QVBoxLayout *layout;
signals:
public slots:
    void on_debugLog(QString s);
};


#endif // WIDGETS_H
