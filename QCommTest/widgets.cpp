#include <QtTest>
#include "widgets.h"

StringWidget::StringWidget(const char* name) : Transport(this, name)
{
    title = new QLabel(name);
    edit = new QLineEdit("enter text here", this);
    label = new QLabel("display text here", this);
    layout = new QVBoxLayout;
    layout->addWidget(title);
    layout->addWidget(edit);
    layout->addWidget(label);
    setLayout(layout);
    connect(this, SIGNAL(update_label(QString)), this, SLOT(on_update_label(QString)));
    connect(edit, SIGNAL(returnPressed()), this, SLOT(on_returnPressed()));
}

StringWidget::~StringWidget()
{
    delete edit;
    delete label;
    delete title;
    delete layout;
}

void StringWidget::on_update_label(QString s)
{
    label->setText(s);
}

void StringWidget::on_returnPressed()
{
    QString s = edit->text();
    for(QMap<QString, Channel*>::iterator it=channels.begin(); it!=channels.end(); ++it)
    {
        it.value()->invoke("update_label(QString)", s);
        QTest::qWait(20);
    }
    notifySubscribers("update_log(QString)", s);                    //all StringWidgets will log their strings
}




LogWidget::LogWidget(const char* name) : Transport(this, name)
{
    label = new QLabel(name);
    edit = new QTextEdit(this);
    layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(edit);
    setLayout(layout);
    connect(this, SIGNAL(update_log(QString)), this, SLOT(on_update_log(QString)));
}
LogWidget::~LogWidget()
{
    delete label;
    delete edit;
    delete layout;
}
void LogWidget::on_update_log(QString s)
{
    edit->append(s);
}




DebugLogWidget::DebugLogWidget(const char* name) : Transport(this, name)
{
    label = new QLabel(name);
    edit = new QTextEdit(this);
    layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(edit);
    setLayout(layout);
    connect(this, SIGNAL(debugLog(QString)), this, SLOT(on_debugLog(QString)));
}
DebugLogWidget::~DebugLogWidget()
{
    delete label;
    delete edit;
    delete layout;
}
void DebugLogWidget::on_debugLog(QString s)
{
    edit->append(s);
}
