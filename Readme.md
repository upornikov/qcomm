# qcomm - communication module (Qt4)
 - qcomm provides Transport class, which allows peer-to-peer communication between those, who inherit from it; 
 - through the connection channel, remote invocation of exposed peer methods is available; 
 - peers can subscribe for each other's signals and by declaring corresponding handlers follow their peers' signal emissions;
 - qcomm is implemented in C++ Qt and Python Qt;

# PyQCommTest - simple demo of qcomm module in python
 - ServerWidget and ClientWidget classes inherit from qcomm.Transport to enable duplex communication between them.
 - ServerWidget starts listening on localhost:9988. Every new instance of the ClientWidget opens channel to the ServerWidget via that socket.
 - On every new connect, the ServerWidget subscribes for debugLog and statusLog signals from the ClientWidget and shows corresponding messages, when signals are emitted by ClientWidgets.
 - ClientWidget is a url processor, which encapsulates WebView widget to show web pages.
 - Work sequence as follows:
   - ServerWidget triggers ClientWidget.processUrl via remote invocation mechanism;
   - ClientWidget loads the page, then emits corresonding debugLog and statusLog signals: subscription mechanism notifies subscribers about these events;
   - Via subscription channel, ServerWidget receives the log messages from the client and prints them;
   
# QCommTest - simple demo of qcomm module in C++
 - Demo of remote signal invocation and subscription functionality between various widgets of a C++ test application.
 